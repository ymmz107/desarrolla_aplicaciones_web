**Desarrollo de aplicaciones web con base de datos**
**5to semestre**
**Morales Zayas Yael Magdiel**

- Practica #1 - 02/09/2022 - Practicas de ejemplo
Comit: 96f7dc476da25e239889954fb14c76020154fb1b
Archivo: https://gitlab.com/ymmz107/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html

- Practica #2 - 09/09/2022 - Practica mensaje JavaScript
Comit: 69f27cbbccf9831c856cf76ca2a439332b1ac480
Archivo: https://gitlab.com/ymmz107/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaJavaScript.html

- Practica #3 - 15/09/2022 - Práctica web con bases de datos -parte 1
Comit: c59a10aefbe3c17228ceb9aab03d8a96e769d74b
Archivo: https://gitlab.com/ymmz107/desarrolla_aplicaciones_web/-/blob/main/parcial1/practicaWebDatos.rar

- Practica #4 - 19/09/2022 - Vista de consulta de datos (Solo frontend) - 19/09/2022 - Visita de consulta de datos
Comit: e87d565793d44c3eecca210bbe12f4b12551afe4
Archivo: https://gitlab.com/ymmz107/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaWebDatos/consultarDatos.php

- Practica #5 - 19/09/2022 - Vista de consulta de datos (Solo frontend) - 19/09/2022 - Visita de consulta de datos
Comit: 9aebdb1c4123fd6fdf6c0c2134f40b178a70fd10
Archivo: https://gitlab.com/ymmz107/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaWebDatos/registrarDatos.html

- Practica #6 - 23/09/2022 - Practica Web con bases de datos - Mostrar registros en consulta de datos
consultarDatos.php COMMIT: 438a671586146263645c9ed9f478d3bedfdac01e
conexion.php COMMIT: 415b1772d32dfa5ab158346d7914dfa73bdef381
